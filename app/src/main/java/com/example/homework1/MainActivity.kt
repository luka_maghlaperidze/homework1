package com.example.homework1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init(){


        generateRandomNumberButton.setOnClickListener {

            val num1:Int = randNum()
        d("ButtonClicked","$num1")
            randomNumberTextView.text = num1.toString()

            if (num1 > 0 && num1 % 5 == 0) {YesOrNo.text = "Yes"}
            else {YesOrNo.text = "No"}

        }



    }

    private fun randNum():Int{
     val num:Int =  (-100..100).random()
        return num
    }


}